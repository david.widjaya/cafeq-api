import base64
import tempfile

from weasyprint import HTML
from werkzeug.debug.repr import dump
from werkzeug.exceptions import HTTPException
from flask import json, Response, render_template, make_response
from app import app
from config import Config
from app.controller import EmployeeController, AuthController, MenuController, CategoryController, EventController, \
    OrderController, ReportController, NotificationController
from flask import request
from app import response
from app.model.employee import Employee
from functools import wraps

@app.route('/download/report/pdf/<type>', methods=['GET'])
def download_report(type):
    return ReportController.getReport(type)

@app.route('/download/bill/<id_order>', methods=['GET'])
def download_bill(id_order):
    return ReportController.getBill(id_order)

def authorization(f):
    @wraps(f)
    def check():
        headers = request.headers
        bearer = headers.get('Authorization')  # Bearer YourTokenHere
        if bearer is None:
            return response.success(401, 'failed', None, 'Unauthorized')

        token = bearer.split()[1]

        checkToken = Employee.query.filter_by(bearer_token=token).first()
        if checkToken is None:
            return response.success(401, 'failed', None, 'Unauthorized')
        return f()
    return check



@app.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "code": e.code,
        'status': 'failed',
        "message": e.name,
        "data": e.description,
    })
    response.content_type = "application/json"
    return response

@app.route('/')
def index():
    return 'Hello: ' + Config.SQLALCHEMY_DATABASE_URI

@app.route('/employee', methods=['POST'])
def getAllEmployee():
    return EmployeeController.getAllEmployee()

@app.route('/employee/<id>', methods=['GET'])
def getEmployeeById(id):
    return EmployeeController.getEmployeeById(id)

@app.route('/profile', methods=['POST'])
# @authorization
def getProfile():
    return EmployeeController.getProfile()

@app.route('/employee/store', methods=['POST'])
def storeEmployee():
    return EmployeeController.storeEmployee()

@app.route('/employee/delete', methods=['POST'])
def deleteEmployee():
    return EmployeeController.deleteEmployee()

@app.route('/employee/file', methods=['POST'])
def uploadFile():
    return EmployeeController.uploadFile()

@app.route('/employee/update', methods=['POST'])
def updateEmployee():
    return EmployeeController.updateEmployee()

@app.route('/profile/update', methods=['POST'])
def updateProfile():
    return EmployeeController.updateProfile()


@app.route('/menu', methods=['POST'])
def getAllMenu():
    return MenuController.getAllMenu()

@app.route('/menu/id', methods=['POST'])
def getMenuById():
    return MenuController.getMenuById()

@app.route('/menu/store', methods=['POST'])
def storeMenu():
    return MenuController.storeMenu()

@app.route('/menu/update', methods=['POST'])
def updateMenu():
    return MenuController.updateMenu()

@app.route('/menu/delete', methods=['POST'])
def deleteMenu():
    return MenuController.deleteMenu()

@app.route('/category', methods=['POST'])
def getAllCategory():
    return CategoryController.getAllCategory()

@app.route('/category/store', methods=['POST'])
def storeCategory():
    return CategoryController.storeCategory()

@app.route('/order/detail', methods=['POST'])
def orderDetail():
    return OrderController.getOrderDetail()

@app.route('/category/delete', methods=['POST'])
def deleteCategory():
    return CategoryController.deleteCategory()


@app.route('/event', methods=['POST'])
def getAllEvent():
    return EventController.getAllEvent()

@app.route('/event/discount', methods=['POST'])
def getEvent():
    return EventController.getEvent()

@app.route('/event/store', methods=['POST'])
def storeEvent():
    return EventController.storeEvent()

@app.route('/event/delete', methods=['POST'])
def deleteEvent():
    return EventController.deleteEvent()

@app.route('/order', methods=['POST'])
def getAllOrder():
    return OrderController.getAllOrder()

@app.route('/myorder', methods=['POST'])
def getOrderByEmployee():
    return OrderController.getOrderByEmployee()

@app.route('/order/store', methods=['POST'])
def storeOrder():
    return OrderController.storeOrder()

@app.route('/history/login', methods=['POST'])
def historyLogin():
    return EmployeeController.historyLogin()

@app.route('/login', methods=['POST'])
def login():
    return AuthController.login()

@app.route('/notification', methods=['POST'])
def getAllNotification():
    return NotificationController.getAllNotification()
@app.route('/notification/read', methods=['POST'])
def readNotification():
    return NotificationController.readNotification()
@app.route('/notification/read/all', methods=['POST'])
def readAllNotification():
    return NotificationController.readAllNotification()

@app.route('/debug-sentry')
def trigger_error():
    division_by_zero = 1 / 0




def formatArray(data):
    array = []

    for i in data:
        array.append(singleObject(i))
    return array

def singleObject(data):
    data = {
        'id_emp': data.id_emp,
        'name_emp': data.name_emp,
        'email_emp': data.email_emp,
        'password_emp': base64.b64decode(data.password_emp),
        'phone_emp': data.phone_emp,
        'status': data.status,
    }
    return data