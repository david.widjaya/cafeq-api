import werkzeug
import json

from app.model.category_menu import CategoryMenu
from app.model.employee import Employee
from app.model.menu_event import Menu, Event
from app import response, app, db
from flask import request, make_response, jsonify, session
from flask_restful import Api, Resource, reqparse

res = {}


def getAllCategory():
    try:
        category = CategoryMenu.query
        page = int(request.form.get('page')) if request.form.get('page') else 1
        category = category.paginate(page=page, per_page=1000)

        paginate = {
            'prev_page' : category.prev_num,
            'current_page' : category.page,
            'next_page' : category.next_num,
            'total_page' : category.pages,
            'has_next': category.has_next,
            'has_prev': category.has_prev,
        }
        array = []

        for i in category.items:
            array.append(singleObjectCategory(i))

        data = array
        res['category'] = data
        res['paginate'] = paginate
        return response.success(200, 'success', res, "Data Category Has Been Found.")
    except werkzeug.exceptions as e:
        return response.handleError(e)

def storeCategory():
    try:

        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        parser = reqparse.RequestParser()
        parser.add_argument('name_cate_menu', required=True, location='form', help='Name field is required')
        parser.add_argument('desc_cate_menu', required=True, location='form', help='Description field is required')
        args = parser.parse_args()

        name_cate_menu = request.form.get('name_cate_menu')
        desc_cate_menu = request.form.get('desc_cate_menu')

        categoryCheck = CategoryMenu.query.filter_by(name_cate_menu=name_cate_menu).first()
        if categoryCheck:
            return response.success(400, 'failed', None, 'Menu with ' + name_cate_menu + ' has already exist!')

        category = CategoryMenu(
            name_cate_menu=name_cate_menu,
            desc_cate_menu=desc_cate_menu,
            created_by=emp.first().id_emp,
        )
        db.session.add(category)
        db.session.commit()
        return response.success(200, 'success', category.toDict(), "Data Category Has Been Added Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)

def deleteCategory():
    try:
        parser = reqparse.RequestParser()
        parser.add_argument('id_cate_menu', required=True, location='form', help='ID field is required')
        args = parser.parse_args()

        id_cate_menu = request.form.get('id_cate_menu')

        categoryCheck = CategoryMenu.query.get(id_cate_menu)
        if categoryCheck is None:
            return response.success(400, 'failed', None, 'Category has been not found!')

        hasMenu = Menu.query.filter_by(cate_menu_id=id_cate_menu).first()

        if hasMenu:
            return response.success(400, 'failed', None, 'Category has already menu exist!')

        db.session.delete(categoryCheck)
        db.session.commit()
        return response.success(200, 'success', categoryCheck.toDict(), "Data Category Has Been Deleted Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)

def formatArray(data):
    array = []

    for i in data:
        array.append(singleObject(i))
    return array

def singleObject(data):
    data = {
        'id_menu' : data.id_menu,
        'name_menu' : data.name_menu,
        'desc_menu' : data.desc_menu,
        'stock_menu' : data.stock_menu,
        'price_menu' : data.price_menu,
        'category' : data.name_cate_menu,
    }
    return data



def singleObjectCategory(data):
    data = {
        'id_cate_menu' : data.id_cate_menu,
        'name_cate_menu' : data.name_cate_menu,
        'desc_cate_menu' : data.desc_cate_menu,
        'total_cate_menu' : data.total_cate_menu,
    }
    return data

