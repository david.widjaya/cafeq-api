import werkzeug
import json

from weasyprint import HTML

from app.model.category_menu import CategoryMenu
from app.model.employee import Employee
from app.model.menu_event import Menu, Event
from app import response, app, db
from flask import request, make_response, jsonify, session, render_template
from flask_restful import Api, Resource, reqparse
from sqlalchemy import or_
from datetime import date, datetime

from app.model.order import Order
from app.model.order_detail import OrderDetail

res = {}

def getBill(id_order):
    global event
    event = None
    try:
        today = datetime.today().strftime("%m/%d/%Y, %H:%M:%S")
        app.logger.info('%s REPORT_TODAY:' + today)

        # if request.headers.get('bearer_token'):
        #     token = request.headers.get('bearer_token')
        #     app.logger.info('%s TOKEN' + token)
        #     emp = Employee.query.filter_by(bearer_token=token)
        #
        #     if emp.first() is None:
        #         return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        # else:
        #     return response.success(404, "failed", None, 'Unauthorized')

        # emp = Employee.query.all()

        res = {}
        # id_order = request.form.get('id_order')
        # id_order = 82

        order = Order.query.get(id_order)
        if order.event_id:
            event = Event.query.get(order.event_id)

        operator = Employee.query.get(order.created_by).name_emp

        orderDetail = OrderDetail.query.filter(OrderDetail.order_id == id_order).all()

        arr = []
        for detail in orderDetail:
            temp = detail.toDict()
            menu = Menu.query.get( temp['menu_id'] )
            temp['name_menu'] = menu.name_menu
            temp['price_menu'] = menu.price_menu
            temp['logo_menu'] = menu.logo_menu
            arr.append(temp)

        res['order'] = order.toDict()
        if event:
            res['order']['promo_code'] = event.promo_code
        res['orderdetail'] = arr
        res['today'] = today
        res['operator'] = operator
        # return res
        html = render_template('bill.html', res=res)
        html = HTML(string=html)
        result = html.write_pdf()
        res = make_response(result)
        res.headers['Content-Type'] = 'application/pdf'
        res.headers['Content-Disposition'] = \
            'inline; filename=%s.pdf' % 'download'
        return res

    except werkzeug.exceptions as e:
        return response.handleError(e)


def getReport(type):
    try:
        today = datetime.today().strftime('%Y-%m-%d')
        app.logger.info('%s REPORT_TODAY:' + today)

        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            app.logger.info('%s TOKEN' + token)
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        if type is not None:
            app.logger.info('%s ada type' + type)
        if type == 'login':
            emp = Employee.query.all()
            html = render_template('report_login.html', emp=emp)
            html = HTML(string=html)
            result = html.write_pdf()
            res = make_response(result)
            res.headers['Content-Type'] = 'application/pdf'
            res.headers['Content-Disposition'] = \
                'inline; filename=%s.pdf' % 'download'
            return res

        if type == 'order':
            # order = Order.query.all()

            order = Order.query \
                .join(Employee, Order.created_by == Employee.id_emp) \
                .add_columns(Order.id_order, Order.created_at, Order.total_order, Order.payment_order,
                             Order.changes_order, Employee.name_emp).all()
            # ar = []
            # for it in order:
            #     ar.append({
            #         'name' : it.name_emp,
            #     })
            # return response.success(404, "failed", ar, 'Data Profile Has Been Not Found.')

            html = render_template('report_order.html', order=order)
            html = HTML(string=html)
            result = html.write_pdf()
            res = make_response(result)
            res.headers['Content-Type'] = 'application/pdf'
            res.headers['Content-Disposition'] = \
                'inline; filename=%s.pdf' % 'download'
            return res
        # return response.success(200, 'success', res, "Data Menu Has Been Found.")
    except werkzeug.exceptions as e:
        return response.handleError(e)


def storeMenu():
    try:

        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        parser = reqparse.RequestParser()
        parser.add_argument('name_menu', required=True, location='form', help='Name field is required')
        # parser.add_argument('desc_menu', required=True, location='form', help='Description field is required')
        parser.add_argument('stock_menu', required=True, location='form', help='Stock field is required')
        parser.add_argument('price_menu', required=True, location='form', help='Price field is required')
        parser.add_argument('cate_menu_id', required=True, location='form', help='Category field is required')
        args = parser.parse_args()

        name_menu = request.form.get('name_menu')
        desc_menu = request.form.get('desc_menu')
        stock_menu = request.form.get('stock_menu')
        price_menu = request.form.get('price_menu')
        cate_menu_id = request.form.get('cate_menu_id')
        emp_id = emp.first().id_emp

        menuCheck = Menu.query.filter_by(name_menu=name_menu).first()
        if menuCheck:
            return response.success(400, 'failed', None, 'Menu with ' + name_menu + ' has already exist!')

        menu = Menu(
            name_menu=name_menu,
            desc_menu=desc_menu,
            stock_menu=stock_menu,
            price_menu=price_menu,
            cate_menu_id=cate_menu_id,
            created_by=emp_id,
        )
        db.session.add(menu)
        db.session.commit()
        return response.success(200, 'success', menu.toDict(), "Data Menu Has Been Added Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)


def deleteMenu():
    try:
        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        parser = reqparse.RequestParser()
        parser.add_argument('id_menu', required=True, location='form', help='Name field is required')
        args = parser.parse_args()

        id_menu = request.form.get('id_menu')

        menuCheck = Menu.query.get(id_menu)
        historyOrder = OrderDetail.query.filter_by(menu_id=id_menu).first()
        if historyOrder:
            return response.success(400, 'failed', None,
                                    'Menu with ' + menuCheck.name_menu + ' has already transaction!')

        db.session.delete(menuCheck)
        db.session.commit()
        return response.success(200, 'success', menuCheck.toDict(), "Data Menu Has Been Deleted Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)


def getMenuById():
    try:
        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        parser = reqparse.RequestParser()
        parser.add_argument('id_menu', required=True, location='form', help='Menu ID field is required')
        id_menu = request.form.get('id_menu')
        args = parser.parse_args()
        menu = Menu.query \
            .filter_by(id_menu=id_menu) \
            .join(CategoryMenu, Menu.cate_menu_id == CategoryMenu.id_cate_menu) \
            .add_columns(Menu.id_menu, Menu.name_menu, Menu.desc_menu, Menu.stock_menu, Menu.price_menu,
                         CategoryMenu.id_cate_menu, CategoryMenu.name_cate_menu, CategoryMenu.id_cate_menu).first()
        # return menu.name_menu
        array = []
        array.append({
            'id_menu': menu.id_menu,
            'name_menu': menu.name_menu,
            'desc_menu': menu.desc_menu,
            'stock_menu': menu.stock_menu,
            'price_menu': menu.price_menu,
            'category': menu.name_cate_menu,
            'id_cate_menu': menu.id_cate_menu,
        })
        res['menu'] = array
        return response.success(200, 'success', res, "Data Menu Has Been Found.")
    except werkzeug.exceptions as e:
        return response.handleError(e)


def updateMenu():
    try:
        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        parser = reqparse.RequestParser()
        parser.add_argument('id_menu', required=True, location='form', help='Menu ID field is required')
        parser.add_argument('name_menu', required=True, location='form', help='Name field is required')
        parser.add_argument('desc_menu', required=True, location='form', help='Description field is required')
        parser.add_argument('stock_menu', required=True, location='form', help='Stock field is required')
        parser.add_argument('price_menu', required=True, location='form', help='Price field is required')
        parser.add_argument('cate_menu_id', required=True, location='form', help='Category field is required')
        args = parser.parse_args()
        # return json.dumps(request.form.items())
        id_menu = request.form.get('id_menu')
        name_menu = request.form.get('name_menu')
        desc_menu = request.form.get('desc_menu')
        stock_menu = request.form.get('stock_menu')
        price_menu = request.form.get('price_menu')
        cate_menu_id = request.form.get('cate_menu_id')

        menuCheck = Menu.query.filter_by(name_menu=name_menu).first()
        if menuCheck:
            return response.success(400, 'failed', None, 'Menu with ' + name_menu + ' has already exist!')

        form = request.form
        arr = {}
        for key in form.keys():
            for item in form.getlist(key):
                arr[key] = item
        arr['updated_by'] = emp.first().id_emp
        update = Menu.query.filter_by(id_menu=id_menu)
        update.update(arr)
        update = Menu.query.filter_by(id_menu=id_menu).first()
        db.session.add(update)
        db.session.commit()
        return response.success(200, 'success', update.toDict(), "Data Menu Has Been Updated Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)


def formatArray(data):
    array = []

    for i in data:
        array.append(singleObject(i))
    return array


def singleObject(data):
    data = {
        'id_menu': data.id_menu,
        'name_menu': data.name_menu,
        'desc_menu': data.desc_menu,
        'stock_menu': data.stock_menu,
        'price_menu': data.price_menu,
        'category': data.name_cate_menu,
    }
    return data
