from datetime import datetime

import werkzeug
import json

from app.model.employee import Employee
from app.model.menu_event import Menu, Event
from app.model.notification import Notification
from app.model.order import Order
from app.model.order_detail import OrderDetail
from app import response, app, db
from flask import request, make_response, jsonify, session
from flask_restful import Api, Resource, reqparse
import json

res = {}


def getAllOrder():
    try:
        order = Order.query
        page = int(request.form.get('page')) if request.form.get('page') else 1
        order = order.paginate(page=page, per_page=1000)

        paginate = {
            'prev_page': order.prev_num,
            'current_page': order.page,
            'next_page': order.next_num,
            'total_page': order.pages,
            'has_next': order.has_next,
            'has_prev': order.has_prev,
        }
        data = formatArray(order.items)
        res['order'] = data
        res['paginate'] = paginate
        return response.success(200, 'success', res, "Data Order Has Been Found.")
    except werkzeug.exceptions as e:
        return response.handleError(e)


def getOrderByEmployee():
    try:

        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        order = Order.query.filter_by(created_by=emp.first().id_emp)
        order = order.order_by(Order.id_order.desc())
        page = int(request.form.get('page')) if request.form.get('page') else 1
        order = order.paginate(page=page, per_page=1000)

        paginate = {
            'prev_page': order.prev_num,
            'current_page': order.page,
            'next_page': order.next_num,
            'total_page': order.pages,
            'has_next': order.has_next,
            'has_prev': order.has_prev,
        }
        data = formatHistory(order.items)
        res['order'] = data
        res['paginate'] = paginate
        return response.success(200, 'success', res, "Data Order Has Been Found.")
    except werkzeug.exceptions as e:
        return response.handleError(e)


def getOrderDetail():
    try:
        event = None
        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        parser = reqparse.RequestParser()
        parser.add_argument('id_order', required=True, location='form', help='ID Order field is required')

        args = parser.parse_args()

        id_order = request.form.get('id_order')

        order = Order.query.get(id_order)
        if order.event_id:
            event = Event.query.get(order.event_id)

        orderDetail = OrderDetail.query.filter(OrderDetail.order_id == id_order).all()

        arr = []
        for detail in orderDetail:
            temp = detail.toDict()
            menu = Menu.query.get( temp['menu_id'] )
            temp['name_menu'] = menu.name_menu
            temp['price_menu'] = menu.price_menu
            temp['logo_menu'] = menu.logo_menu
            arr.append(temp)

        res['order'] = order.toDict()
        if event:
            res['order']['promo_code'] = event.promo_code
        res['orderdetail'] = arr
        return response.success(200, 'success', res, "Data Order Has Been Found.")
    except werkzeug.exceptions as e:
        return response.handleError(e)


def storeOrder():
    try:
        # headers = request.headers
        # bearer = headers.get('Authorization')  # Bearer YourTokenHere
        # token = bearer.split()[1]

        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        parser = reqparse.RequestParser()
        parser.add_argument('total_order', required=True, location='form', help='Total Order field is required')
        parser.add_argument('payment_order', required=True, location='form', help='Payment field is required')
        parser.add_argument('changes_order', required=True, location='form', help='Changes field is required')
        parser.add_argument('status_order', required=True, location='form', help='Status Order field is required')
        parser.add_argument('order_detail', required=True, location='form', help='Order Detail field is required')
        parser.add_argument('customer_name', required=True, location='form', help='Customer field is required')

        args = parser.parse_args()

        event_id = request.form.get('event_id')
        total_order = request.form.get('total_order')
        payment_order = request.form.get('payment_order')
        changes_order = request.form.get('changes_order')
        status_order = request.form.get('status_order')
        customer_name = request.form.get('customer_name')
        order_detail = request.form.get('order_detail')
        emp_id = Employee.query.filter_by(bearer_token=token).first().id_emp
        order_detail = json.loads(order_detail)  # convert json.stringify to Dict
        # return order_detail[0]
        app.logger.info('%s orderdetail ', order_detail)

        notif = {}
        stockmenu = 0
        namemenu = ""

        for detail in order_detail:
            app.logger.info('%s foreach ', 'xxx')
            menu = Menu.query.get(detail['menu_id'])
            app.logger.info('%s namemenu ', menu.name_menu)
            stockmenu = menu.stock_menu
            namemenu = menu.name_menu
            app.logger.info('%s namemenuafter ', namemenu)

            if int(detail['qty_order_detail']) >= menu.stock_menu:
                return response.success(400, "failed", None, 'Data Order Quantity More Than Stock.')
            else:
                menu.stock_menu = stockmenu - int(detail['qty_order_detail'])
                db.session.commit()

        if stockmenu <= 10:
            notif = Notification(
                title_notif='Stock Warning!',
                desc_notif='Stock for ' + namemenu + ' less than 10 items, please restock your food!',
                read_status=0,
                created_at=datetime.utcnow()
            )
            db.session.add(notif)
            db.session.commit()
        order = Order(
            event_id=event_id,
            total_order=total_order,
            customer_name=customer_name,
            payment_order=payment_order,
            changes_order=changes_order,
            status_order=status_order,
            created_by=emp_id,
        )
        db.session.add(order)
        db.session.commit()

        for detail in order_detail:
            orDetail = OrderDetail(
                order_id=order.id_order,
                menu_id=detail['menu_id'],
                price_order=detail['price_order'],
                qty_order_detail=detail['qty_order_detail'],
                subtotal_order_detail=detail['subtotal_order_detail'],
            )
            db.session.add(orDetail)
            db.session.commit()

        # menuCheck = Menu.query.filter_by(name_menu=name_menu).first()
        # if menuCheck:
        #     return response.success(400, 'failed', None, 'Menu with ' + name_menu + ' has already exist!')
        #
        # menu = Menu(
        #     name_menu=name_menu,
        #     desc_menu=desc_menu,
        #     stock_menu=stock_menu,
        #     price_menu=price_menu,
        #     cate_menu_id=cate_menu_id,
        #     created_by=emp_id,
        # )

        # # res = []
        # # for item in order.toDict():
        # #     res.append({
        # #     'event_id':item.event_id,
        # #     'total_order':item.total_order,
        # #     'payment_order':item.payment_order,
        # #     'changes_order':item.changes_order,
        # #     'status_order':item.status_order,
        # #     'created_by':item.emp_id,
        # #     })
        # # if notif is not None:
        # #     for item in notif.toDict():
        # #         res.append({
        # #             'id_notif' : item.id_notif,
        # #             'title_notif' : item.title_notif,
        # #         })
        #
        # order.
        if bool(notif):
            notif = notif.toDict()
        notif['id_order'] = order.id_order
        return response.success(200, 'success', notif, "Data Order Has Been Added Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)


# def updateMenu():
#     try:
#         parser = reqparse.RequestParser()
#         parser.add_argument('name_menu', required=True, location='form', help='Name field is required')
#         parser.add_argument('desc_menu', required=True, location='form', help='Description field is required')
#         parser.add_argument('stock_menu', required=True, location='form', help='Stock field is required')
#         parser.add_argument('price_menu', required=True, location='form', help='Price field is required')
#         parser.add_argument('cate_menu_id', required=True, location='form', help='Category field is required')
#         parser.add_argument('emp_id', required=True, location='form', help='Employee field is required')
#         args = parser.parse_args()
#         # return json.dumps(request.form.items())
#         name_menu = request.form.get('name_menu')
#         desc_menu = request.form.get('desc_menu')
#         stock_menu = request.form.get('stock_menu')
#         price_menu = request.form.get('price_menu')
#         cate_menu_id = request.form.get('cate_menu_id')
#         emp_id = request.form.get('emp_id')
#
#         menuCheck = Menu.query.filter_by(name_menu=name_menu).first()
#         if menuCheck:
#             return response.success(400, 'failed', None, 'Menu with ' + name_menu + ' has already exist!')
#
#         form = request.form
#         arr = {}
#         for key in form.keys():
#             for item in form.getlist(key):
#                 if key == 'emp_id':
#                     arr['updated_by'] = item
#                 else:
#                     arr[key] = item
#         update = Menu.query.filter_by(id_menu=3)
#         update.update(arr)
#         update = Menu.query.filter_by(id_menu=3).first()
#         return response.success(200, 'success', update.toDict(), "Data Menu Has Been Updated Succesfully.")
#     except werkzeug.exceptions as e:
#         return response.handleError(e)


def formatArray(data):
    array = []

    for i in data:
        array.append(singleObject(i))
    return array


def formatHistory(data):
    array = []

    for i in data:
        array.append({
            'id_order': i.id_order,
            'total_order': i.total_order,
            'payment_order': i.payment_order,
            'status_order': i.status_order,
            'created_at': i.created_at,
        })
    return array


def singleObject(data):
    data = {
        'id_order': data.id_order,
        'total_order': data.total_order,
        'payment_order': data.payment_order,
        'status_order': data.status_order,
    }
    return data
