import werkzeug
import json

from app.model.category_menu import CategoryMenu
from app.model.employee import Employee
from app.model.menu_event import Menu, Event
from app import response, app, db
from flask import request, make_response, jsonify, session
from flask_restful import Api, Resource, reqparse

from app.model.order import Order
from app.model.order_detail import OrderDetail

res = {}


def getAllEvent():
    try:
        event = Event.query
        q = request.form.get('q')
        if q is not None:
            search = "%{}%".format(q)
            event = event.filter(Event.name_event.like(search))
        page = int(request.form.get('page')) if request.form.get('page') else 1
        event = event.paginate(page=page, per_page=1000)

        paginate = {
            'prev_page' : event.prev_num,
            'current_page' : event.page,
            'next_page' : event.next_num,
            'total_page' : event.pages,
            'has_next': event.has_next,
            'has_prev': event.has_prev,
        }
        data = formatArray(event.items)
        res['event'] = data
        res['paginate'] = paginate
        return response.success(200, 'success', res, "Data Event Has Been Found.")
    except werkzeug.exceptions as e:
        return response.handleError(e)

def deleteEvent():
    try:
        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        parser = reqparse.RequestParser()
        parser.add_argument('id_event', required=True, location='form', help='Name field is required')
        args = parser.parse_args()

        id_event = request.form.get('id_event')

        event = Event.query.get(id_event)
        historyOrder = Order.query.filter_by(event_id=id_event).first()
        if historyOrder:
            return response.success(400, 'failed', None, 'Event with ' + event.name_event + ' has already transaction!')

        db.session.delete(event)
        db.session.commit()
        return response.success(200, 'success', event.toDict(), "Data Event Has Been Deleted Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)

def getEvent():
    try:
        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        parser = reqparse.RequestParser()
        parser.add_argument('promo_code', required=True, location='form', help='Promo code field is required')
        parser.add_argument('total', required=True, location='form', help='Total field is required')
        args = parser.parse_args()

        promo_code = request.form.get('promo_code')
        total = request.form.get('total')
        event = Event.query.filter_by(promo_code=promo_code).first()
        if event is None:
            return response.success(400, 'failed', None, 'Event with ' + promo_code + ' Has Been Not Found!')
        min_price = event.min_price

        app.logger.info('%s total ', int(total))
        app.logger.info('%s min ', int(min_price))

        if int(total) < int(min_price):
            app.logger.info('lower ')
            app.logger.info('%s total ', int(total))
            return response.success(400, 'failed', None, 'Minimum Transaction Is Equals or More Than ' + str(min_price) + '!')

        return response.success(200, 'success', event.toDict(), "Data Event Has Been Found.")
    except werkzeug.exceptions as e:
        return response.handleError(e)

def storeEvent():
    try:
        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token).first()

            if emp is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')

        parser = reqparse.RequestParser()
        parser.add_argument('name_event', required=True, location='form', help='Name field is required')
        parser.add_argument('status_event', required=True, location='form', help='Status field is required')
        parser.add_argument('promo_code', required=True, location='form', help='Promo Code field is required')
        parser.add_argument('date_start_event', required=True, location='form', help='Date Start field is required')
        parser.add_argument('date_end_event', required=True, location='form', help='Date End field is required')
        parser.add_argument('desc_event', required=True, location='form', help='Description field is required')
        parser.add_argument('min_price', required=True, location='form', help='Minimum price field is required')
        parser.add_argument('discount_event', required=True, location='form', help='Discount field is required')
        parser.add_argument('max_discount', required=True, location='form', help='Max discount field is required')
        args = parser.parse_args()

        name_event = request.form.get('name_event')
        promo_code = request.form.get('promo_code')
        desc_event = request.form.get('desc_event')
        status_event = request.form.get('status_event')
        date_start_event = request.form.get('date_start_event')
        date_end_event = request.form.get('date_end_event')
        min_price = request.form.get('min_price')
        discount_event = request.form.get('discount_event')
        max_discount = request.form.get('max_discount')
        menu_id = request.form.getlist('menu_id[]')
        # return str(emp_id)

        eventCheck = Event.query.filter_by(name_event=name_event, promo_code=promo_code).first()
        if eventCheck:
            return response.success(400, 'failed', None, 'Menu with ' + eventCheck.name_event + '|' + eventCheck.promo_code + ' has already exist!')

        event = Event(
            name_event=name_event,
            promo_code=promo_code,
            desc_event=desc_event,
            status_event=status_event,
            date_start_event=date_start_event,
            date_end_event=date_end_event,
            min_price=min_price,
            discount_event=discount_event,
            created_by=emp.id_emp,
            max_discount=max_discount
        )
        db.session.add(event)
        db.session.commit()
        return response.success(200, 'success', event.toDict(), "Data Event Has Been Added Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)

def formatArray(data):
    array = []

    for i in data:
        array.append(singleObject(i))
    return array

def singleObject(data):
    data = {
        'id_event' : data.id_event,
        'name_event' : data.name_event,
        'desc_event' : data.desc_event,
        'promo_code' : data.promo_code,
        'discount_event' : data.discount_event,
        'max_discount' : data.max_discount,
    }
    return data