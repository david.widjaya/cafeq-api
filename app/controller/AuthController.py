import base64
from datetime import datetime

import werkzeug
import json

from app.model.employee import Employee
from app import response, app, db
from flask import request, make_response, jsonify
from flask_restful import Api, Resource, reqparse
from sqlalchemy import update

from app.utils.generate_key import bearer_token

res = {}


def login():
    try:
        parser = reqparse.RequestParser()
        parser.add_argument('username_emp', required=True, location='form', help='Username field is required')
        parser.add_argument('password_emp', required=True, location='form', help='Password field is required')
        args = parser.parse_args()

        # check if user exist
        username_emp = request.form.get('username_emp')
        password_emp = request.form.get('password_emp')
        emp = Employee.query.filter_by(username_emp=request.form.get('username_emp')).first()
        if emp is None:
            return response.success(404, "failed", None, 'Login Failed.')

        if password_emp == base64.b64decode(emp.password_emp):
            empUpdate = Employee.query.filter_by(username_emp=request.form.get('username_emp')).update(dict(bearer_token=bearer_token()))
            db.session.commit()
            return response.success(200, 'success', emp.toDict(), "Login Succesfully.")

        return response.success(401, 'failed', None, "Login Failed.")
    except werkzeug.exceptions as e:
        return response.handleError(e)

def getEmployeeById(id):
    try:

        emp = Employee.query.get(id)
        if emp is None:
            return response.success(404, "failed", None, 'Data Employee Has Been Not Found.')

        # return emp
        # data = formatArray(emp)
        return response.success(200,"success", emp.toDict(), 'Data Employee Has Been Found')
    except werkzeug.exceptions as e:
        return response.handleError(e)

def storeEmployee():
    try:
        parser = reqparse.RequestParser()
        parser.add_argument('username_emp', required=True, location='form', help='Name field is required')
        parser.add_argument('password_emp', required=True, location='form', help='Password field is required')
        args = parser.parse_args()

        username_emp = request.form.get('username_emp')
        password_emp = request.form.get('password_emp')

        usernameCheck = Employee.query.filter_by(username_emp=username_emp).first()
        if usernameCheck:
            return response.success(400, 'failed', None, 'Username with ' + username_emp + ' has already exist!')

        emp = Employee(
            username_emp=username_emp,
            password_emp=password_emp,
            created_at=datetime.utcnow()
        )
        db.session.add(emp)
        db.session.commit()
        return response.success(200, 'success', emp.toDict(), "Data Employee Has Been Added Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)

def formatArray(data):
    array = []

    for i in data:
        array.append(singleObject(i))
    return array

def singleObject(data):
    data = {
        'id_emp' : data.id_emp,
        'name_emp' : data.name_emp,
        'email_emp' : data.email_emp,
        'password_emp' : data.password_emp,
    }
    return data