import werkzeug
import json

from app.model.category_menu import CategoryMenu
from app.model.employee import Employee
from app.model.menu_event import Menu, Event
from app import response, app, db
from flask import request, make_response, jsonify, session
from flask_restful import Api, Resource, reqparse
from sqlalchemy import or_
import os

from app.model.order import Order
from app.model.order_detail import OrderDetail

res = {}


def getAllMenu():
    try:

        q = request.form.get('q')
        cate_menu_id = request.form.get('cate_menu_id')
        menu = Menu.query\
            .join(CategoryMenu, Menu.cate_menu_id == CategoryMenu.id_cate_menu)\
            .add_columns(Menu.id_menu, Menu.logo_menu, Menu.name_menu, Menu.desc_menu, Menu.stock_menu, Menu.price_menu,
                         CategoryMenu.name_cate_menu, CategoryMenu.id_cate_menu)
        if q is not None:
            search = "%{}%".format(q)
            menu = menu.filter(Menu.name_menu.like(search))
            # menu = Menu.query \
            #     .join(CategoryMenu, Menu.cate_menu_id == CategoryMenu.id_cate_menu) \
            #     .add_columns(Menu.id_menu, Menu.name_menu, Menu.desc_menu, Menu.stock_menu, Menu.price_menu,
            #                  CategoryMenu.name_cate_menu, Menu.cate_menu_id).filter(Menu.name_menu.like(search))

        if cate_menu_id != -1 and cate_menu_id is not None:
            menu = menu.filter_by(id_cate_menu=cate_menu_id)


        page = int(request.form.get('page')) if request.form.get('page') else 1
        menu = menu.paginate(page=page, per_page=1000)

        paginate = {
            'prev_page': menu.prev_num,
            'current_page': menu.page,
            'next_page': menu.next_num,
            'total_page': menu.pages,
            'has_next': menu.has_next,
            'has_prev': menu.has_prev,
        }
        data = formatArray(menu.items)
        res['menu'] = data
        res['paginate'] = paginate
        return response.success(200, 'success', res, "Data Menu Has Been Found.")
    except werkzeug.exceptions as e:
        return response.handleError(e)


def storeMenu():
    try:

        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        parser = reqparse.RequestParser()
        parser.add_argument('name_menu', required=True, location='form', help='Name field is required')
        # parser.add_argument('desc_menu', required=True, location='form', help='Description field is required')
        parser.add_argument('stock_menu', required=True, location='form', help='Stock field is required')
        parser.add_argument('price_menu', required=True, location='form', help='Price field is required')
        parser.add_argument('cate_menu_id', required=True, location='form', help='Category field is required')
        args = parser.parse_args()

        name_menu = request.form.get('name_menu')
        desc_menu = request.form.get('desc_menu')
        stock_menu = request.form.get('stock_menu')
        price_menu = request.form.get('price_menu')
        cate_menu_id = request.form.get('cate_menu_id')
        emp_id = emp.first().id_emp

        category = CategoryMenu.query.get(cate_menu_id)
        category.total_cate_menu = category.total_cate_menu + 1
        # db.session.commit()

        menuCheck = Menu.query.filter_by(name_menu=name_menu).first()
        if menuCheck:
            return response.success(400, 'failed', None, 'Menu with ' + name_menu + ' has already exist!')

        file = request.files['img']
        # if(posted):
        filename = file.filename
        file.save(os.path.join(app.config['UPLOAD_FOLDER_MENU'], filename))


        menu = Menu(
            name_menu=name_menu,
            desc_menu=desc_menu,
            stock_menu=stock_menu,
            price_menu=price_menu,
            cate_menu_id=cate_menu_id,
            created_by=emp_id,
            logo_menu='image_menu/'+filename,
        )
        db.session.add(menu)
        db.session.commit()
        return response.success(200, 'success', menu.toDict(), "Data Menu Has Been Added Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)

def deleteMenu():
    try:
        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        parser = reqparse.RequestParser()
        parser.add_argument('id_menu', required=True, location='form', help='Name field is required')
        args = parser.parse_args()

        id_menu = request.form.get('id_menu')

        menuCheck = Menu.query.get(id_menu)
        if menuCheck is None:
            return response.success(404, 'failed', None, 'Menu has been not found!')

        historyOrder = OrderDetail.query.filter_by(menu_id=id_menu).first()
        if historyOrder:
            return response.success(400, 'failed', None, 'Menu with ' + menuCheck.name_menu + ' has already transaction!')

        db.session.delete(menuCheck)
        db.session.commit()
        return response.success(200, 'success', menuCheck.toDict(), "Data Menu Has Been Deleted Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)


def getMenuById():
    try:
        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        parser = reqparse.RequestParser()
        parser.add_argument('id_menu', required=True, location='form', help='Menu ID field is required')
        id_menu = request.form.get('id_menu')
        args = parser.parse_args()
        menu = Menu.query \
            .filter_by(id_menu=id_menu)\
            .join(CategoryMenu, Menu.cate_menu_id == CategoryMenu.id_cate_menu)\
            .add_columns(Menu.id_menu, Menu.logo_menu, Menu.name_menu, Menu.desc_menu, Menu.stock_menu, Menu.price_menu,
                         CategoryMenu.id_cate_menu,CategoryMenu.name_cate_menu, CategoryMenu.id_cate_menu).first()
        # return menu.name_menu
        array = []
        array.append({
            'id_menu': menu.id_menu,
            'name_menu': menu.name_menu,
            'desc_menu': menu.desc_menu,
            'stock_menu': menu.stock_menu,
            'price_menu': menu.price_menu,
            'category': menu.name_cate_menu,
            'id_cate_menu': menu.id_cate_menu,
            'logo_menu': menu.logo_menu,
        })
        res['menu'] = array
        return response.success(200, 'success', res, "Data Menu Has Been Found.")
    except werkzeug.exceptions as e:
        return response.handleError(e)

def updateMenu():
    try:
        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        parser = reqparse.RequestParser()
        parser.add_argument('id_menu', required=True, location='form', help='Menu ID field is required')
        parser.add_argument('name_menu', required=True, location='form', help='Name field is required')
        parser.add_argument('desc_menu', required=True, location='form', help='Description field is required')
        parser.add_argument('stock_menu', required=True, location='form', help='Stock field is required')
        parser.add_argument('price_menu', required=True, location='form', help='Price field is required')
        parser.add_argument('cate_menu_id', required=True, location='form', help='Category field is required')
        args = parser.parse_args()
        # return json.dumps(request.form.items())
        id_menu = request.form.get('id_menu')
        name_menu = request.form.get('name_menu')
        desc_menu = request.form.get('desc_menu')
        stock_menu = request.form.get('stock_menu')
        price_menu = request.form.get('price_menu')
        cate_menu_id = request.form.get('cate_menu_id')

        # menuCheck = Menu.query.filter_by(name_menu=name_menu).first()
        # if menuCheck:
        #     return response.success(400, 'failed', None, 'Menu with ' + name_menu + ' has already exist!')
        app.logger.info('%s total ', request.files['img'])

        file = request.files['img']
        filename = file.filename
        file.save(os.path.join(app.config['UPLOAD_FOLDER_MENU'], filename))

        form = request.form
        arr = {}
        for key in form.keys():
            for item in form.getlist(key):
                arr[key] = item
        arr['updated_by'] = emp.first().id_emp
        arr['logo_menu'] = 'image_menu/'+filename
        update = Menu.query.filter_by(id_menu=id_menu)
        update.update(arr)
        update = Menu.query.filter_by(id_menu=id_menu).first()
        db.session.add(update)
        db.session.commit()
        return response.success(200, 'success', update.toDict(), "Data Menu Has Been Updated Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)


def formatArray(data):
    array = []

    for i in data:
        array.append(singleObject(i))
    return array


def singleObject(data):
    data = {
        'id_menu': data.id_menu,
        'name_menu': data.name_menu,
        'desc_menu': data.desc_menu,
        'stock_menu': data.stock_menu,
        'price_menu': data.price_menu,
        'category': data.name_cate_menu,
        'logo_menu': data.logo_menu,
    }
    return data
