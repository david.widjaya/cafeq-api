import base64
from datetime import datetime

import werkzeug
import json

from app.model.employee import Employee
from app import response, app, db
from flask import request, make_response, jsonify
from flask_restful import Api, Resource, reqparse
from sqlalchemy import update

from app.model.notification import Notification
from app.utils.generate_key import bearer_token

res = {}


def getAllNotification():
    try:
        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token).first()

            if emp is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        notif = Notification.query.all()

        arr = []
        for item in notif:
            arr.append({
                'id_notif' : item.id_notif,
                'title_notif' : item.title_notif,
                'desc_notif' : item.desc_notif,
                'read_status' : item.read_status,
                'created_at' : item.created_at,
            })
        return response.success(200, "success", arr, 'Data Notification Has Been Found.')

    except werkzeug.exceptions as e:
        return response.handleError(e)

def readNotification():
    try:
        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token).first()

            if emp is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        parser = reqparse.RequestParser()
        parser.add_argument('id_notification', required=True, location='form', help='Id field is required')
        args = parser.parse_args()

        id_notification = request.form.get('id_notification')
        notif = Notification.query.get(id_notification)

        if notif is None:
            return response.success(404, "failed", None, 'Data Notification Has Been Not Found.')

        notif.read_status = 1
        db.session.commit()
        return response.success(200, "success", notif.toDict(), 'Data Notification Has Been Read.')

    except werkzeug.exceptions as e:
        return response.handleError(e)

def readAllNotification():
    try:
        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token).first()

            if emp is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        notif = Notification.query.all()
        for item in notif:
            item.read_status = 1
        db.session.commit()
        return response.success(200, "success", None, 'Data All Notification Has Been Read.')

    except werkzeug.exceptions as e:
        return response.handleError(e)