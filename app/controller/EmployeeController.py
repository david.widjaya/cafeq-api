import base64
from datetime import datetime

import werkzeug
import json

from app.model.employee import Employee
from app import response, app, db
from flask import request, make_response, jsonify
from flask_restful import Api, Resource, reqparse
import os
res = {}


def getAllEmployee():
    try:
        page = int(request.form.get('page')) if request.form.get('page') else 1

        query = "%{}%".format( request.form.get('query') if request.form.get('query') is not None else '')
        emp = Employee.query.filter(Employee.name_emp.like(query)).paginate(page=page, per_page=1000)


        paginate = {
            'prev_page' : emp.prev_num,
            'current_page' : emp.page,
            'next_page' : emp.next_num,
            'total_page' : emp.pages,
            'has_next': emp.has_next,
            'has_prev': emp.has_prev,
        }
        data = formatArray(emp.items)
        res['employee'] = data
        res['paginate'] = paginate
        return response.success(200, 'success', res, "Data Employee Has Been Found.")
    except werkzeug.exceptions as e:
        return response.handleError(e)

def getEmployeeById(id):
    try:
        # parser = reqparse.RequestParser()
        # parser.add_argument('id', required=True, location='args', help='Id field is required')
        # args = parser.parse_args()

        emp = Employee.query.get(id)
        if emp is None:
            return response.success(404, "failed", None, 'Data Employee Has Been Not Found.')

        # return emp
        # data = formatArray(emp)
        return response.success(200,"success", emp.toDict(), 'Data Employee Has Been Found')
    except werkzeug.exceptions as e:
        return response.handleError(e)

def getProfile():
    try:
        # parser = reqparse.RequestParser()
        # parser.add_argument('id', required=True, location='args', help='Id field is required')
        # args = parser.parse_args()
        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token).first()

            if emp is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
            return response.success(200,"success", emp.toDict(), 'Data Profile Has Been Found')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

    except werkzeug.exceptions as e:
        return response.handleError(e)

def uploadFile():
    try:
        if request.method=='POST':
            file = request.files['file']
            # if(posted):
            filename = file.filename
            file.save(os.path.join(app.config['UPLOAD_FOLDER_USER'], filename))
            return file.filename
            print('posted', posted)
        # return 'Gagal'
    except werkzeug.exceptions as e:
        return response.handleError(e)


def storeEmployee():
    try:
        parser = reqparse.RequestParser()
        parser.add_argument('username_emp', required=True, location='form', help='Username field is required')
        parser.add_argument('password_emp', required=True, location='form', help='Password field is required')
        parser.add_argument('role_type_emp', required=True, location='form', help='Role field is required')
        parser.add_argument('status', required=True, location='form', help='Status field is required')
        args = parser.parse_args()

        username_emp = request.form.get('username_emp')
        password_emp = request.form.get('password_emp')
        name_emp = request.form.get('name_emp') if request.form.get('name_emp') is not None else ''
        role_type_emp = request.form.get('role_type_emp')
        status = request.form.get('status')
        password_emp = base64.b64encode(password_emp)

        usernameCheck = Employee.query.filter_by(username_emp=username_emp).first()
        if usernameCheck:
            return response.success(400, 'failed', None, 'Username with ' + username_emp + ' has already exist!')

        file = request.files['img']
        filename = file.filename
        file.save(os.path.join(app.config['UPLOAD_FOLDER_USER'], filename))
        emp = Employee(
            password_emp=password_emp,
            username_emp=username_emp,
            name_emp=name_emp,
            role_type_emp=role_type_emp,
            status=status,
            logo_emp='logo_emp/'+filename,
            created_at=datetime.utcnow()
        )
        db.session.add(emp)
        db.session.commit()
        return response.success(200, 'success', emp.toDict(), "Data Employee Has Been Added Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)


def deleteEmployee():
    try:
        parser = reqparse.RequestParser()
        parser.add_argument('id_emp', required=True, location='form', help='Id field is required')
        args = parser.parse_args()

        id_emp = request.form.get('id_emp')

        emp = Employee.query.get(id_emp)
        if emp is None:
            return response.success(400, 'failed', None, 'Employee Has Been Not Found!')

        db.session.delete(emp)
        db.session.commit()
        return response.success(200, 'success', emp.toDict(), "Data Employee Has Been Deleted Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)


def updateEmployee():
    try:
        parser = reqparse.RequestParser()
        # parser.add_argument('name_emp', required=True, location='form', help='Name field is required')
        parser.add_argument('password_emp', required=True, location='form', help='Password field is required')
        parser.add_argument('phone_emp', required=True, location='form', help='Phone field is required')
        parser.add_argument('emp_id', required=True, location='form', help='Employee field is required')
        # parser.add_argument('id', required=True, location='form', help='ID field is required')

        args = parser.parse_args()
        # name_emp = request.form.get('name_emp')
        password_emp = request.form.get('password_emp')
        phone_emp = request.form.get('phone_emp')
        emp_id = request.form.get('emp_id')
        # id = request.form.get('id')

        empCheck = Employee.query.filter_by(id_emp=emp_id)
        if empCheck is None:
            return response.success(400, 'failed', None, 'Employee has been not found!')

        file = request.files['img']
        filename = file.filename
        file.save(os.path.join(app.config['UPLOAD_FOLDER_USER'], filename))

        form = request.form
        arr = {}
        for key in form.keys():
            for item in form.getlist(key):
                if key == 'emp_id':
                    continue
                if key == 'password_emp':
                    arr['password_emp'] = base64.b64encode(item)
                else:
                    arr[key] = item
        arr['logo_emp'] = 'logo_emp/'+filename

        # update = Employee.query.filter_by(id_emp=id)
        empCheck.update(arr)
        db.session.commit()
        return response.success(200, 'success', empCheck.first().toDict(), "Data Employee Has Been Updated Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)

def updateProfile():
    try:

        if request.headers.get('bearer_token'):
            token = request.headers.get('bearer_token')
            emp = Employee.query.filter_by(bearer_token=token)

            if emp.first() is None:
                return response.success(404, "failed", None, 'Data Profile Has Been Not Found.')
        else:
            return response.success(404, "failed", None, 'Unauthorized')

        parser = reqparse.RequestParser()
        parser.add_argument('address_emp', required=True, location='form', help='Address field is required')
        parser.add_argument('phone_emp', required=True, location='form', help='Phone field is required')
        # parser.add_argument('active', required=True, location='form', help='Status field is required')
        args = parser.parse_args()
        # address_emp = request.form.get('address_emp')
        # phone_emp = request.form.get('phone_emp')

        file = request.files['img']
        filename = file.filename
        file.save(os.path.join(app.config['UPLOAD_FOLDER_USER'], filename))

        form = request.form
        arr = {}
        for key in form.keys():
            for item in form.getlist(key):
                arr[key] = item
        arr['logo_emp'] = 'logo_emp/'+filename

        emp.update(arr)

        db.session.commit()
        # update = Employee.query.filter_by(bearer_token=token).first()
        return response.success(200, 'success', emp.first().toDict(), "Data Profile Has Been Updated Succesfully.")
    except werkzeug.exceptions as e:
        return response.handleError(e)

def historyLogin():
    try:
        emp = Employee.query.all()
        page = int(request.form.get('page')) if request.form.get('page') else 1
        emp = Employee.query.paginate(page=page, per_page=1000)

        paginate = {
            'prev_page' : emp.prev_num,
            'current_page' : emp.page,
            'next_page' : emp.next_num,
            'total_page' : emp.pages,
            'has_next': emp.has_next,
            'has_prev': emp.has_prev,
        }
        array = []

        for i in emp.items:
            array.append({
                'name_emp': i.name_emp,
                'last_login_at': i.last_login_at,
            })
        res['history'] = array
        res['paginate'] = paginate
        return response.success(200, 'success', res, "Data History Login Has Been Found.")
    except werkzeug.exceptions as e:
        return response.handleError(e)

def formatArray(data):
    array = []

    for i in data:
        array.append(singleObject(i))
    return array

def singleObject(data):
    data = {
        'id_emp' : data.id_emp,
        'name_emp' : data.name_emp,
        'email_emp' : data.email_emp,
        'password_emp' : base64.b64decode(data.password_emp),
        'status' : data.status,
        'phone_emp' : data.phone_emp,
        'logo_emp' : data.logo_emp,
    }
    return data