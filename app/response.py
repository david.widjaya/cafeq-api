from flask import jsonify, make_response
def success(code, status, values, message):
    res  = {
        'code' : code,
        'status': status,
        'message': message,
        'data' : values,
    }

    return make_response(jsonify(res), 200)

def badRequest(code, status, values, message):
    res  = {
        'code' : code,
        'status': status,
        'message' : message,
        'data': values,
    }
    return make_response(jsonify(res), 400)

def handleError(e):
    res  = {
        'code' : e.code,
        'status': 'failed',
        'message': e.name,
        'data' : e.description,
    }
    return make_response(jsonify(res), e.code)
