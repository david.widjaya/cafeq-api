from sqlalchemy import inspect
from datetime import datetime

from app import db

class Employee(db.Model):
    __tablename__ = 'tb_employee'

    id_emp = db.Column(db.BigInteger, primary_key=True)
    name_emp = db.Column(db.String(255), nullable=True)
    username_emp = db.Column(db.String(255), nullable=True)
    email_emp = db.Column(db.String(255), nullable=True)
    password_emp = db.Column(db.String(255), nullable=True)
    phone_emp = db.Column(db.BigInteger, nullable=True)
    address_emp = db.Column(db.String(255), nullable=True)
    role_type_emp = db.Column(db.Integer, nullable=True)
    logo_emp = db.Column(db.Text, nullable=True)
    bearer_token = db.Column(db.String(255), nullable=True)
    remember_token = db.Column(db.String(255), nullable=True)
    status = db.Column(db.String(255), nullable=True)
    created_at = db.Column(db.DateTime, nullable=True, default=datetime.utcnow())
    updated_at = db.Column(db.DateTime, nullable=True, onupdate=datetime.utcnow())
    deleted_at = db.Column(db.DateTime, nullable=True)
    last_login_at = db.Column(db.DateTime, nullable=True, onupdate=datetime.utcnow())

    def toDict(self):
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}

    #
    # @property
    # def serialize(self):
    #     return {
    #         'id_emp': self.id_emp,
    #         'name_emp': self.name_emp,
    #         'email_emp': self.email_emp,
    #         'password_emp': self.password_emp,
    #         'phone_emp': self.phone_emp,
    #         'address_emp': self.address_emp,
    #         'role_type_emp': self.role_type_emp,
    #         'logo_emp': self.logo_emp,
    #         'bearer_token': self.bearer_token,
    #         'remember_token': self.remember_token,
    #         'created_at': self.created_at,
    #         'deleted_at': self.deleted_at,
    #         'last_login_at': self.last_login_at,
    #     }
