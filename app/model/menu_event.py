from sqlalchemy.orm import relationship

from app import db
from sqlalchemy import inspect, Column, ForeignKey, String
from datetime import datetime
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

# tags = db.Table('menu_event',
#     db.Column('menu_id', db.BigInteger, db.ForeignKey('tb_menu.id_menu'), primary_key=True),
#     db.Column('event_id', db.BigInteger, db.ForeignKey('tb_event.id_event'), primary_key=True),
#     menu = db.relationship("Menu", back_populates="events"),
#     event = db.relationship("Event", back_populates="menu"),
# )

# class MenuEvent(db.Model):
#     __tablename__ = 'menu_event'
#     menu_id = Column(ForeignKey('tb_menu.id_menu'), primary_key=True)
#     event_id = Column(ForeignKey('tb_event.id_event'), primary_key=True)
#     menu = relationship("Menu", back_populates="events")
#     event = relationship("Event", back_populates="menu")



class Menu(db.Model):
    __tablename__ = 'tb_menu'

    id_menu = db.Column(db.BigInteger, primary_key=True)
    # cate_menu_id = db.Column(db.BigInteger, db.ForeignKey('cate_menu_id'), nullable=False)
    cate_menu_id = db.Column(db.BigInteger, db.ForeignKey('tb_category_menu.id_cate_menu'), nullable=False)
    # category_menu = db.relationship('CategoryMenu', backref=db.backref('menu', lazy=True))
    name_menu = db.Column(db.String(255), nullable=False)
    logo_menu = db.Column(db.Text, nullable=True)
    desc_menu = db.Column(db.Text, nullable=True)
    stock_menu = db.Column(db.BigInteger, nullable=False)
    price_menu = db.Column(db.BigInteger, nullable=False)
    created_by = db.Column(db.Integer, nullable=True)
    updated_by = db.Column(db.Integer, nullable=True)
    created_at = db.Column(db.DateTime, nullable=True, default=datetime.utcnow())
    updated_at = db.Column(db.DateTime, nullable=True, onupdate=datetime.utcnow())
    deleted_at = db.Column(db.DateTime, nullable=True)
    # events = relationship("MenuEvent", back_populates="menu")

    def __repr__(self):
        return '<Menu %r>' % self.desc_menu

    def toDict(self):
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}

class Event(db.Model):
    __tablename__ = 'tb_event'

    id_event = db.Column(db.BigInteger, primary_key=True)
    name_event = db.Column(db.String(255), nullable=False)
    desc_event = db.Column(db.Text, nullable=True)
    promo_code = db.Column(db.Text, nullable=False)
    status_event = db.Column(db.Integer, nullable=False)
    date_start_event = db.Column(db.DateTime, nullable=False)
    date_end_event = db.Column(db.DateTime, nullable=False)
    min_price = db.Column(db.BigInteger, nullable=False)
    max_discount = db.Column(db.BigInteger, nullable=False)
    discount_event = db.Column(db.BigInteger, nullable=False)
    created_by = db.Column(db.Integer, nullable=True)
    updated_by = db.Column(db.Integer, nullable=True)
    created_at = db.Column(db.DateTime, nullable=True, default=datetime.utcnow())
    updated_at = db.Column(db.DateTime, nullable=True, onupdate=datetime.utcnow())
    deleted_at = db.Column(db.DateTime, nullable=True)
    # menu = relationship("MenuEvent", back_populates="event")

    def __repr__(self):
        return '<Event %r>' % self.desc_event

    def toDict(self):
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}

