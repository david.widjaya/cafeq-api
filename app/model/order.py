from datetime import datetime

from sqlalchemy import inspect

from app import db

class Order(db.Model):
    __tablename__ = 'tb_order'

    id_order = db.Column(db.BigInteger, primary_key=True)
    event_id = db.Column(db.BigInteger, db.ForeignKey('tb_event.id_event'), nullable=True)
    total_order = db.Column(db.BigInteger, nullable=False)
    payment_order = db.Column(db.BigInteger, nullable=False)
    changes_order = db.Column(db.BigInteger, nullable=False)
    status_order = db.Column(db.BigInteger, nullable=False)
    customer_name = db.Column(db.String(255), nullable=False)
    created_by = db.Column(db.Integer, nullable=True)
    updated_by = db.Column(db.Integer, nullable=True)
    created_at = db.Column(db.DateTime, nullable=True, default=datetime.utcnow())
    updated_at = db.Column(db.DateTime, nullable=True,  onupdate=datetime.utcnow())
    deleted_at = db.Column(db.DateTime, nullable=True)

    def toDict(self):
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}