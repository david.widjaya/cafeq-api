from app import db
from datetime import datetime
from flask_sqlalchemy import inspect
class CategoryMenu(db.Model):
    __tablename__ = 'tb_category_menu'

    id_cate_menu = db.Column(db.BigInteger, primary_key=True)
    name_cate_menu = db.Column(db.String(255), nullable=False)
    desc_cate_menu = db.Column(db.Text, nullable=True)
    total_cate_menu = db.Column(db.BigInteger, nullable=True, default=0)
    created_by = db.Column(db.Integer, nullable=True)
    updated_by = db.Column(db.Integer, nullable=True)
    created_at = db.Column(db.DateTime, nullable=True, default=datetime.utcnow())
    updated_at = db.Column(db.DateTime, nullable=True, onupdate=datetime.utcnow())
    deleted_at = db.Column(db.DateTime, nullable=True)
    menu = db.relationship('Menu', backref='categorymenus', lazy=True)

    def __repr__(self):
        return '<CategoryMenu %r>' % self.desc_cate_menu

    def toDict(self):
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}

    # @property
    # def serialize(self):
    #     return {
    #         'id_cate_menu': self.id_cate_menu,
    #         'name_cate_menu': self.name_cate_menu,
    #         'desc_cate_menu': self.desc_cate_menu,
    #         'total_cate_menu': self.total_cate_menu,
    #         'created_by': self.created_by,
    #         'updated_by': self.updated_by,
    #         'created_at': self.created_at,
    #         'updated_at': self.updated_at,
    #         'deleted_at': self.deleted_at,
    #     }
