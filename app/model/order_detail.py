from datetime import datetime

from sqlalchemy import inspect

from app import db

class OrderDetail(db.Model):
    __tablename__ = 'tb_order_detail'

    id_order_detail = db.Column(db.BigInteger, primary_key=True)
    order_id = db.Column(db.BigInteger, db.ForeignKey('tb_order.id_order'), nullable=False)
    menu_id = db.Column(db.BigInteger, nullable=False)
    price_order = db.Column(db.BigInteger, nullable=False)
    qty_order_detail = db.Column(db.BigInteger, nullable=False)
    subtotal_order_detail = db.Column(db.BigInteger, nullable=False)
    created_at = db.Column(db.DateTime, nullable=True, default=datetime.utcnow())
    updated_at = db.Column(db.DateTime, nullable=True,  onupdate=datetime.utcnow())
    deleted_at = db.Column(db.DateTime, nullable=True)


    def toDict(self):
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}