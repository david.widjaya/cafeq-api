from app import db

class Event(db.Model):
    __tablename__ = 'tb_event'

    id_event = db.Column(db.BigInteger, primary_key=True)
    name_event = db.Column(db.String(255), nullable=False)
    desc_event = db.Column(db.Text, nullable=False)
    status_event = db.Column(db.Integer, nullable=False)
    date_start_event = db.Column(db.DateTime, nullable=False)
    date_end_event = db.Column(db.DateTime, nullable=False)
    agreement_event = db.Column(db.Text, nullable=False)
    min_product_event = db.Column(db.BigInteger, nullable=False)
    discount_event = db.Column(db.BigInteger, nullable=False)
    created_by = db.Column(db.Integer, nullable=False)
    updated_by = db.Column(db.Integer, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=False)
    deleted_at = db.Column(db.DateTime, nullable=False)