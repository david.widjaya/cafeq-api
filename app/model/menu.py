from app import db

class Menu(db.Model):
    __tablename__ = 'tb_menu'

    id_menu = db.Column(db.BigInteger, primary_key=True)
    # cate_menu_id = db.Column(db.BigInteger, db.ForeignKey('cate_menu_id'), nullable=False)
    cate_menu_id = db.Column(db.BigInteger, db.ForeignKey('tb_category_menu.id_cate_menu'), nullable=False)
    # category_menu = db.relationship('CategoryMenu', backref=db.backref('menu', lazy=True))
    name_menu = db.Column(db.String(255), nullable=False)
    desc_menu = db.Column(db.Text, nullable=False)
    stock_menu = db.Column(db.BigInteger, nullable=False)
    price_menu = db.Column(db.BigInteger, nullable=False)
    created_by = db.Column(db.Integer, nullable=False)
    updated_by = db.Column(db.Integer, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=False)
    deleted_at = db.Column(db.DateTime, nullable=False)
