from datetime import datetime

from sqlalchemy import inspect

from app import db

class Notification(db.Model):
    __tablename__ = 'tb_notification'

    id_notif = db.Column(db.BigInteger, primary_key=True)
    title_notif = db.Column(db.String(255), nullable=True)
    desc_notif = db.Column(db.Text, nullable=True)
    read_status = db.Column(db.BigInteger, nullable=True)
    created_at = db.Column(db.DateTime, nullable=True, default=datetime.utcnow())

    def toDict(self):
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}