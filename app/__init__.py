from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import logging
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

# sentry_sdk.init(
#     dsn="https://2f98240a0e384138a7e08c88b6477d97@o1131879.ingest.sentry.io/6176911",
#     integrations=[FlaskIntegration()],
#     #
#     # # Set traces_sample_rate to 1.0 to capture 100%
#     # # of transactions for performance monitoring.
#     # # We recommend adjusting this value in production,
#     traces_sample_rate=1.0
# )

app = Flask(__name__)


app.config.from_object('config.Config')
db = SQLAlchemy(app)
migrate = Migrate(app, db)

from app.model import employee, category_menu, order, order_detail, notification
from app import routes
